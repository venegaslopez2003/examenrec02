/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrec02;

/**
 *
 * @author Jesus Alberto Vnegas Lopez 
 */
public class Nomina {
    private int numRecibo;
    private String nombre;
    private int puesto;
    private int nivel;
    private int dias;

    public Nomina() {
        this.numRecibo = 0;
        this.nombre = "";
        this.puesto = 0;
        this.nivel = 0;
        this.dias = 0;
        
    }

    public Nomina(int numRecibo, String nombre, int puesto, int nivel, int dias) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.puesto = puesto;
        this.nivel = nivel;
        this.dias = dias;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }
    
    
   
    float calcularPago(){
        float pago = 0;
     
    if (this.puesto == 1) {
        pago = 100; 
    } else if (this.puesto == 2) {
        pago  = 200; 
    } else if (this.puesto== 3) {
       pago = 300; 
    }
    
        return pago * this.dias;
    }
    float calcularImpuesto(){
        
    float pago = 0;
    if (this.nivel == 1) {
        pago = this.calcularPago()* .05f; 
    } else if (this.nivel == 2) {
        pago = this.calcularPago() * .03f; 
  
    }
    return pago ; 
        
      
    }
    float calcularTotal(){
        float total =0;
        total=this.calcularPago() - this.calcularImpuesto();
        
        return total;
    }
    
    

    
    
    
    
    
    
}
